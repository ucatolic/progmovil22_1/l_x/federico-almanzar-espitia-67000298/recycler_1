package com.sigmotoa.characters

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Recupera los datos de la fuente de datos
        val characterList = DataSource(this).getCharacterList()

        val recyclerView:RecyclerView=findViewById(R.id.reciclerview)
        recyclerView.adapter=CharacterAdapter(characterList)
    }
}