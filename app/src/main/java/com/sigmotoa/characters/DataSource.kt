package com.sigmotoa.characters

import android.content.Context

class DataSource(val context: Context) {
    fun getCharacterList(): Array<String>
    {
        //Retorna la lista de personajes
        return context.resources.getStringArray(R.array.character_array)
    }
}